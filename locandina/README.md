# Locandina

L'evento è incluso nel progetto DOORS perciò si utilizzerà la locandina del progetto DOORS e l'hashtag #doors

Altri hashtag #LinuxDay2021 #linuxday #pdp #fabriano #comunità #digitale #privacy #makerspace #spicyfabriano


## Informazioni di base

- [x] Data: 2021-10-23
- [x] Ore: 15:00
- [x] Luogo fisico: Biblioteca Multimediale Romualdo Sassi - Sala Dalmazio Pilati
- [x] Ulteriori info: [https://bit.ly/fabriano-linux-day-2021](https://bit.ly/fabriano-linux-day-2021) o [codice QR](https://gitlab.com/pdpfsug/proj/linux-day-2021/-/blob/main/locandina/assets/qrc-linux-day-2021.png)

## Titolo

Linux Day 2021 - da 18 anni, un appuntamento per la comunità favrianese ;-)

## Sottotitolo

[NOTA per Vale: questo sottotitolo è legato al logo. Se appare nel logo si può anche omettere]
Dati, dati, dati… Ma dati a chi?

## Descrizione

Nella cornice nazionale del LINUX DAY - https://linuxday.it

[LOGO NAZIONALE recuperato dallo zip](https://www.linuxday.it/2021/promo/2021_webkit.zip)

GIORNATA DIVULGATIVA SU GNU/LINUX, SOFTWARE LIBERO E LIERTÀ DIGITALI OGGI.

Per informazioni sull'evento fabrianese: 
[https://bit.ly/fabriano-linux-day-2021](https://bit.ly/fabriano-linux-day-2021) o scansiona il codice QR.

## PROGRAMMA 15:00 - 19:00

ATTO 1: IO DESIDERIO

1. 15:00 - 15:15 Accoglienza con musica libera
2. 15:20 - 15:35 Costin Dragoi - Introduzione al Software Libero
3. 15:35 - 15:55 Marco Agostinelli - Fare musica con Software Libero
4. 15:55 - 16:10 Andrei Calapod - Il MakerSpace e il risveglio del mio desiderio
4. 16:10 - 16:30 Lorenzo Armezzani - Cittadini digitali 1: di chi sono i dati? 
-- pausa --

ATTO 2: IO TERRITORIO

5. 16:40 - 17:00 Costin Dragoi - Ora basta, archivio i miei dati su Nextcloud!
6. 17:00 - 17:20 Luca Ferroni - Kanban Board: risolvere i problemi insieme
7. 17:20 - 17:40 Lorenzo Armezzani - Cittadini digitali 2: partecipo perché posso
8. 17:40 - 18:00 Gabriele Burattini - Privacy per ricerche web - Meta Search Engine
9. 18:00 - 18:20 Andrei Calapod - Blender: modellare il bello in 3D
-- pausa --

ATTO 3: IO HACKER

IN DIRETTA SU EVENTO NAZIONALE [NOTA: preferirei non accorciarlo, ma se devi fallo]
https://www.linuxday.it/2021/programma/live.php?slug=elaborated

10. 18:30 - 19:00 Luca Ferroni - IoRestoACasa.work, social hacking da Fabriano al TG1
                                 
## QR Code

- [qrc-linux-day-2021.png](https://gitlab.com/pdpfsug/proj/linux-day-2021/-/blob/main/locandina/assets/qrc-linux-day-2021.png)

## Loghi

tanti...

Questo può andare a destra del titolo 
[Linux Day Italia](https://www.linuxday.it/2021/promo/2021_webkit.zip).

- PDPFSUG
- Loghi DOORS presenti nella locandina Pedagogia del Desiderio - TOGLIERE AXé
- DIGITAL INNOVATION HUB
- Patrocinio del Comune di Fabriano
- Biblioteca multimediale "R. Sassi"

TOGLIERE IL F-ACTORY!
TOGLIERE IL F-ACTORY!
TOGLIERE IL F-ACTORY!
TOGLIERE IL F-ACTORY!

trovi tutto il materiale in [https://gitlab.com/pdpfsug/proj/linux-day-2021/-/tree/main/locandina/assets](https://gitlab.com/pdpfsug/proj/linux-day-2021/-/tree/main/locandina/assets)
