# Linee Guida

## Linee Guida Per Diventare Relatore

Per rispondere alle domande scrivi un commento nella [issue](https://gitlab.com/pdpfsug/proj/linux-day-2021/-/issues?scope=all&state=opened&search=talk) intitolata a tuo nome, esempio: `Costin talk`. Se non e' presente creane una.

Se hai bisogno di aiuto contattaci nel [gruppo di lavoro](https://t.me/joinchat/bcttmrwkjM00YjE8) su Telegram.

### Cosa Devi Sapere

- Se verrai in presenza dovrai farti validare il green pass inviando un email a `info@pdp.linux.it`.
- La data per le adesioni come relatore e' aperta fino al 26 Settembre.
- Il talk che presenterai deve essere collegato al tema centrale di questo anno. Il tema centrale di questo anno sono i **dati**. Vedi [piu informazioni](https://www.linuxday.it/2021/).
- Puoi presentare piu di un talk.

### Cosa Dobbiamo Sapere

- **Presenterai online o in presenza?** Sei hai la possibilita di venire in presenza ti preghiamo di farlo. L'evento si terra a Fabriano nella sede di _TODO: da decidere_. Solo le persone con il green pass posso entrare.
- **Quale e' l'argomento che tratterai durante il tuo talk?**
- **Hai un orario di preferenza per quando inziare il talk?** Ne terremo conto durante l'organizzaizone del programma.
- **Quanto sara lungo il talk?** Il tempo deve essere espresso in minuti e non deve superare il limite consentito.

## Linee Guida Per Preparare Il Talk

- L'argomento del talk deve essere collegato al tema centrale, vedi [piu informazioni](https://www.linuxday.it/2021/).
- Il titolo non deve essere piu lungo di 42 caratteri.
- Il tempo massimo di un talk e' di 25 minuti.
- L'argomento, il titolo e la durata del talk devono essere communicati entro il ~~3 ottobre~~.

Se presenti piu talk, il formato e' lo stesso.

### Risorse Che Potrebbero Aiutarti

[Tema centrale del linux day di questo anno](https://www.linuxday.it/2021/).

[Linee guida ufficiali](https://www.linuxday.it/2021/lineeguida/).
