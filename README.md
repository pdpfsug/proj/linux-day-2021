# Linux Day 2021

L'evento si terra il 23 ottobre.

## Tema Centrale

Dati, dati, dati... Ma dati a chi?

Il Linux Day 2021 è dedicato alle informazioni che alimentano il software, ed alle loro infinite fonti, rappresentazioni, trasformazioni e utilizzi: dati personali, dati pubblici, dati privati, dati rubati, dati liberi, big data, linked data, open data e dataset.

Per raccontare le esperienze virtuose, mettere in guardia sugli utilizzi meno virtuosi, esplorare strumenti e piattaforme che ne permettono la gestione, la tutela e la condivisione.

## Relatori

### Costin Dragoi

- [x] Introduzione al software libero [10m]
- [x] Degoogled - Ora basta, uso Nextcloud [20m]

### Luca Ferroni

- [x] Problem solving ai tempi dell'Agile [20m]
- [x] [IoRestoACasa.work](https://iorestoacasa.work/) un percoso di social hacking tutto italiano [20m]

### Anonimo

- [ ] La mia esperenza personale con GNU/Linux [15m]

### Andrei Calapod

- [x] Modellazione 3D con Blender [20m]

### Marco Agostinelli

- [x] Free and Open Source Software per la musica [20m]

### Lorenzo Armezzani

- [x] Leggi sulla privacy e GDPR [20m]
- [x] Piattaforme per la democrazia partecipata [20m]

### Gabriele Burattini

- [x] Privacy Online - Meta Search Engine (searx-ng) [15m]

## Programma

ATTO 1: IO DESIDERIO

1. 15:00 - 15:15 Accoglienza con musica libera
2. 15:20 - 15:35 Costin Dragoi - Introduzione al Software Libero
3. 15:35 - 15:55 Marco Agostinelli - Fare musica con Software Libero
4. 15:55 - 16:10 Andrei Calapod - Il MakerSpace e il risveglio del mio desiderio
4. 16:10 - 16:30 Lorenzo Armezzani - Cittadini digitali 1: di chi sono i dati? 
-- pausa --

ATTO 2: IO TERRITORIO

5. 16:40 - 17:00 Costin Dragoi - Ora basta, archivio i miei dati su Nextcloud!
6. 17:00 - 17:20 Luca Ferroni - Kanban Board: risolvere i problemi insieme
7. 17:20 - 17:40 Lorenzo Armezzani - Cittadini digitali 2: partecipo perché posso
8. 17:40 - 18:00 Gabriele Burattini - Privacy per ricerche web - Meta Search Engine
9. 18:00 - 18:20 Andrei Calapod - Blender: modellare il bello in 3D
-- pausa --

ATTO 3: IO HACKER

IN DIRETTA SU EVENTO NAZIONALE
https://www.linuxday.it/2021/programma/live.php?slug=elaborated

10. 18:30 - 19:00 Luca Ferroni - IoRestoACasa.work, social hacking da Fabriano al TG1
                                 


| Orario | Relatore           | Talk                                                                                        | Minuti | Descrizione                                                                                                                        |
|--------|--------------------|---------------------------------------------------------------------------------------------|:------:|------------------------------------------------------------------------------------------------------------------------------------|
| TBD    | Costin Dragoi      | Introduzione al software libero                                                             | 10     |                                                                                                                                    |
| TBD    | Costin Dragoi      | Degoogled - Ora basta, uso Nextcloud                                                        | 20     |                                                                                                                                    |
| TBD    | Luca Ferroni       | Problem solving ai tempi dell'Agile                                                         | 20     |                                                                                                                                    |
| TBD    | Luca Ferroni       | [IoRestoACasa.work](https://iorestoacasa.work/) un percoso di social hacking tutto italiano | 20     |                                                                                                                                    |
| TBD    | Nicolo Tibet       | La mia esperenza personale con GNU/Linux                                                    | 15     |                                                                                                                                    |
| TBD    | Andrei Calapod     | Modellazione 3D con Blender                                                                 | 20     |                                                                                                                                    |
| TBD    | Marco Agostinelli  | FOSS per la musica                                                                          | 20     | Free and Open Source Software per la muisca, sull'utilizzo del re.corder sotto Linux con Alsa, Jack, Ardour, Musescore e Puredata. |
| TBD    | Lorenzo Armezzani  | Leggi sulla privacy e GDPR                                                                  | 20     |                                                                                                                                    |
| TBD    | Lorenzo Armezzani  | Piattaforme per la democrazia partecipata                                                   | 20     |                                                                                                                                    |
| TBD    | Gabriele Burattini | Privacy Online - Meta Search Engine (searx-ng)                                              | 15     | L'uso di un Meta Search Engine (searx-ng) come strumento di tutela della privacy nelle nostre ricerche online                      |
